import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { NgForm } from "@angular/forms";

import { BeerServiceProvider } from "../../providers/beer-service";
import { GiphyServiceProvider } from "../../providers/giphy-service";


@Component({
  selector: 'page-beer-modal',
  templateUrl: './beer-modal.html'
})
export class BeerModalPage {

    @ViewChild('name') name;
    beer: any = {};
    error: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public beerService: BeerServiceProvider,
                public giphyService: GiphyServiceProvider,
                public viewCtrl: ViewController,
                public toastCtrl: ToastController) {
        if (this.navParams.data.id) {
            this.beerService.get(this.navParams.get('id')).subscribe((beer: any) => {
                this.beer = beer;
                this.beer.href = beer._links.self.href;
                this.giphyService.get(beer.name).subscribe(url => beer.giphyUrl = url);
            });
        }
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    save(form: NgForm) {
        let update: boolean = form['href'];
        this.beerService.save(form).subscribe(result => {
            let toast = this.toastCtrl.create({
                message: 'Beer "' + form.name + '" ' + ((update) ? 'updated' : 'added') + '.',
                duration: 2000
            });
            toast.present();
            this.dismiss();
        }, error => this.error = error)
    }

    ionViewDidLoad() {
        setTimeout(() => {
            this.name.setFocus();
        }, 150);
        // console.log('ionViewDidLoad BeerModalPage');
    }

}
