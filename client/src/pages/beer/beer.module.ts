import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { BeerPage } from './beer';
import { BeerModalPage } from "./beer-modal";
import { BeerServiceProvider } from "../../providers/beer-service";
import { GiphyServiceProvider } from "../../providers/giphy-service";


@NgModule({
    declarations: [
        BeerPage,
        BeerModalPage
    ],
    imports: [
        IonicPageModule.forChild(BeerPage),
    ],
    providers: [
        BeerServiceProvider,
        GiphyServiceProvider
    ],
    entryComponents: [
        BeerModalPage
    ]
})
export class BeerPageModule {
}
