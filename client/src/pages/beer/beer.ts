import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';

import { BeerServiceProvider } from "../../providers/beer-service";
import { GiphyServiceProvider } from "../../providers/giphy-service";
import { BeerModalPage } from "./beer-modal";


@IonicPage()
@Component({
    selector: 'page-beer',
    templateUrl: 'beer.html',
})
export class BeerPage {

    private beers: Array<any>;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public beerService: BeerServiceProvider,
                public giphyService: GiphyServiceProvider,
                public modalCtrl: ModalController,
                public toastCtrl: ToastController) {
    }

    ionViewDidLoad() {
        this.beerService.getGoodBeers().subscribe(beers => {
            this.beers = beers;
            for (const beer of this.beers) {
                this.giphyService.get(beer.name).subscribe(url => {
                    beer.giphyUrl = url;
                });
            }
        })
    }

    openModal(beerId) {
        let modal = this.modalCtrl.create(BeerModalPage, beerId);
        modal.present();
        // refresh data after modal dismissed
        modal.onDidDismiss(() => this.ionViewDidLoad())
    }

}
