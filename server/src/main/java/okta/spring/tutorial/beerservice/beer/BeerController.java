package okta.spring.tutorial.beerservice.beer;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BeerController {

    private BeerRepository repository;

    public BeerController(BeerRepository repository) {
        this.repository = repository;
    }

//    @CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080", "http://localhost:4200", "file://"})
    @GetMapping("/all-beers")
    @CrossOrigin(origins = {"http://localhost:8100","http://localhost:8080"})
    public Collection<Beer> allBears() {
        System.out.println("Responding to a request at \"/all-beers\"");
        return repository.findAll().stream()
                .filter(this::isBeer)
                .collect(Collectors.toList());
    }

    @GetMapping("/good-beers")
    @CrossOrigin(origins = {"http://localhost:8100","http://localhost:8080"})
    public Collection<Beer> goodBeers() {
        System.out.println("Responding to a request at \"/good-beers\"");
        return repository.findAll().stream()
                .filter(this::isGreat)
                .collect(Collectors.toList());
    }

    private boolean isGreat(Beer beer) {
        return !beer.getName().equals("Budweiser") &&
                !beer.getName().equals("Coors Light") &&
                !beer.getName().equals("PBR");
    }

    private boolean isBeer(Beer beer) {
        return true;
    }

}
