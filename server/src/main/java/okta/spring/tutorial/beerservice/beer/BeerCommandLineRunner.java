package okta.spring.tutorial.beerservice.beer;

import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BeerCommandLineRunner implements CommandLineRunner {

    private final BeerRepository repository;

    public BeerCommandLineRunner(BeerRepository repository) {
        this.repository = repository;
    }

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        // Top beers from https://www.beeradvocate.com/lists/top/
        Stream.of("Budweiser", "Kentucky Brunch Brand Stout", "Coors Light", "PBR", "Good Morning",
                "Very Hazy", "King Julius").forEach(name -> repository.save(new Beer(name))
        );
        repository.findAll().forEach(System.out::println);
    }
}
